import React, { Component } from 'react'
import { Footer, FooterTab, Button, Container, Content, Header } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { styles } from './Styles'
import { connect } from 'react-redux'
import { UserProfile, ExamResult } from './index'
 class HomeScreen extends Component {

  render() {
    console.log('nav : ' ,this.props )
    let Userprofile, Examsresult = null
    console.log(this.props.ExamReducer.UserprofileClicked)
    if (this.props.ExamReducer.UserprofileClicked) {
      Userprofile = <UserProfile navigation={this.props.navigate} UserData = {this.props.navigation.state.params ? this.props.navigation.state.params:null}/>
      Examsresult = null

    }
    if (this.props.ExamReducer.ExamsResultclicked) {
      Examsresult = <ExamResult navigation={this.props.navigate} UserData = {this.props.navigation.state.params ? this.props.navigation.state.params : null}/>
      Userprofile = null
    }

    return (
      <Container>
        <Header />
        <Content>
          {Userprofile}
          {Examsresult}
          </Content>
        <Footer >
          <FooterTab style={styles.footertab}>
            <Button style={styles.UserProfileTab} onPress={() => { this.props.setProfileOn() }} >
              <Ionicons size={30} name="md-person" />
            </Button>
            <Button onPress={() => { this.props.setExamResultOn() }} >
              <Ionicons size={30} name="md-book" />
            </Button>
          </FooterTab>
        </Footer>

      </Container>

    )
  }
}

const mapStateToProps = (state) => {
  return {

    ...state

  }
}
const mapDispatchersToProps = (dispatcher) => {
  return {
    setProfileOn: () => dispatcher({ type: 'profileOn' }),
    setExamResultOn: () => dispatcher({ type: 'examResultOn' }),

  }
}
export const Home = connect(mapStateToProps, mapDispatchersToProps)(HomeScreen);

