import React, { Component } from 'react'
import {View} from 'react-native'
import { Table, Row, Rows } from 'react-native-table-component';
import {styles} from './Styles'
export class ExamResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Student Name', 'Math /50', 'English /20'],
      tableData: [
        ['1', '2', '3'],
        ['a', 'b', 'c'],
      ]
    }
  }
  render() {
    console.log('exams' , this.props);

    let SubjectsNames = [];
    let SubjectsGrades = [];
    for(let i =0;i<this.props.UserData.Subjects.length ; i++)
    {
      SubjectsNames.push(this.props.UserData.Subjects[i].SubjectName);
      SubjectsGrades.push(this.props.UserData.Subjects[i].SubjectGrade);
    }
    const state = this.state;
    return (
      <View style={styles.container}>
      <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
        <Row data={SubjectsNames} style={styles.head} textStyle={styles.text}/>
        <Row data={SubjectsGrades} textStyle={styles.text}/>
      </Table>
    </View>
    )
  }
}
