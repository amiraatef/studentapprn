import React, { Component } from 'react'
import {Button,View,Text} from 'native-base'
 import GenerateForm from 'react-native-form-builder'
 import {styles} from './Styles';
 import axios from 'axios';
 const fields = [
  {
    type: 'text',
    name: 'Email',
    required: true,
    icon: 'ios-person',
    label: 'Email',
   
  },
  {
    type: 'password',
    name: 'password',
    icon: 'ios-lock',
    required: true,
    label: 'Password',
  },
  
];
export class Login extends Component {
constructor(props) {
  super(props)

  this.state = {
     Fromerror:false,
     FromerrorMsg:'',
     
  };
};

LoginUser = (LoginUserData) => {

  return new Promise((resolve,reject) => {

      axios.get('https://examsapp-47879.firebaseio.com/students.json')
          .then((response) => {

              
              if(response.data)
              {
                  let responseKeys = Object.keys(response.data);
                  
                  for(let i = 0 ; i < responseKeys.length ; i++)
                  {
                    
                      if(response.data[responseKeys[i]].Email === LoginUserData.Email)
                      {
                        
                          if(response.data[responseKeys[i]].password === LoginUserData.password)
                          {
                            
                               resolve({
                                  LoginState: 'Valid',
                                  UserData: response.data[responseKeys[i]]
                              });
                              
                          }
                      }
                  }
                   resolve({
                      LoginState: 'Not Valid'
                  });
                  return;
              }
          }).catch((error) => reject(error));




  })
  

}


  validate=(field)=>{
    let error = false;
    let errorMsg = '';

    if (field.name === 'Email' && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(field.value)) {
      error = true;
      errorMsg = 'Invalid email address';
    }

    this.setState({Fromerror:true})
    return { error, errorMsg };
  }
  Login() {
    let Newuser = {
      Email: "",
      password: "",
    }
    Newuser = this.formGenerator.getValues();
    let email = Newuser.Email
    let password = Newuser.password
    if(!email.match(/\S/g))
    {
      if(!this.state.FromerrorMsg.match(/\S/g))
      {
      this.setState({FromerrorMsg:'Invaild Email or Password'})
      }
      else{

      this.setState({FromerrorMsg:''})
      }
      return
  }

  this.LoginUser(Newuser).then((response) => {
    
    console.log(response);
    if(response.LoginState === 'Valid')
    {
      this.props.navigation.navigate('Home',response.UserData);
    }
    else{
      alert('Email or/and Password is incorrect');
    }
  })
}
  render() {
    return (
      <View style={styles.container}>
        <View>
  <GenerateForm ref={(c) => {this.formGenerator = c;}}fields={fields} customValidation={(field)=>this.validate(field)} />     
          </View>
              <View style={styles.BtnView}>
          <Button style={styles.submitButton} onPress={() => this.Login()}>
            <Text style={styles.txtbtn} >Login</Text>
          </Button>
          <Button style={styles.submitButton} onPress={() =>this.props.navigation.navigate('SignUp')}>
            <Text style={styles.txtbtn} >SignUp</Text>
          </Button>
          
          <Text>{this.state.FromerrorMsg}</Text>
        </View>
      </View>
    )
  }
}
