import React, { Component } from 'react'
import { Button, View, Text } from 'native-base'
import GenerateForm from 'react-native-form-builder'
import { styles } from './Styles';
import axios from 'axios';
const fields = [
  {
    type: 'text',
    name: 'UserName',
    required: true,
    icon: 'ios-person',
    label: 'User Name',

  },
  {
    type: 'text',
    name: 'Email',
    required: true,
    icon: 'ios-person',
    label: 'Email',

  },
  {
    type: 'password',
    name: 'password',
    icon: 'ios-lock',
    required: true,
    label: 'Password',
  },
  {
    type: 'password',
    name: 'ConfirmPassword',
    icon: 'ios-lock',
    required: true,
    label: 'ConfirmPassword',
  },

];
export class SignUp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      Fromerror: false,
      FromerrorMsg: ''
    };
  };

  SignUpNewUser = (UserData) => {
    if (UserData.password !== UserData.ConfirmPassword)
      return 'Password and Confirm Password must be equal';
    axios.get('https://examsapp-47879.firebaseio.com/students.json')
      .then((response) => {
        if (response.data) {
          let responseKeys = Object.keys(response.data);
          for (let i = 0; i < responseKeys.length; i++) {
            if (response.data[responseKeys[i]].Email === UserData.Email) {
              return 'Email exists before';
            }
          }
        }
        axios.post('https://examsapp-47879.firebaseio.com/students.json', UserData)
          .then((response) => {
            console.log(response)
          }).catch((error) => console.log(error));

      })
      .catch((error) => console.log(error));
  }

  validate = (field) => {
    let error = false;
    let errorMsg = '';

    if (field.name === 'Email' && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(field.value)) {
      error = true;
      errorMsg = 'Invalid email address';
    }

    this.setState({ Fromerror: true })
    return { error, errorMsg };
  }
  Signup(){

      let Newuser = {
        Email: "",
        Password: "",
        UserName: "",
        ConfirmPassword: '',
        Subjects:[
        {SubjectName: 'Arabic' , SubjectGrade: 5},
        {SubjectName: 'English' , SubjectGrade: 15},
        {SubjectName: 'Math' , SubjectGrade: 12},
    ]

      }
      NewUser = this.formGenerator.getValues();
      let email = NewUser.Email
      let password = NewUser.password
      let confirmpassword = Newuser.ConfirmPassword
      if (!email.match(/\S/g) || !password.match(/\S/g)) {
        if (!this.state.FromerrorMsg.match(/\S/g)) {
          this.setState({ FromerrorMsg: 'Invaild Email or Password' })
        }
        else {
          this.setState({ FromerrorMsg: '' })
        }
        return
        
      }
      NewUser.Subjects = [
        {SubjectName: 'Arabic' , SubjectGrade: 5},
        {SubjectName: 'English' , SubjectGrade: 15},
        {SubjectName: 'Math' , SubjectGrade: 12},
    ]
      this.SignUpNewUser(NewUser);
      alert('congrats')
      this.props.navigation.navigate('Login',);


  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <GenerateForm ref={(c) => { this.formGenerator = c; }} fields={fields} customValidation={(field) => this.validate(field)} />
        </View>
        <View style={styles.BtnView}>
          <Button style={styles.submitButton} onPress={() => this.Signup()}>
            <Text style={styles.txtbtn} > Signup</Text>
          </Button>
          <Text>{this.state.FromerrorMsg}</Text>
        </View>
      </View>
    )
  }
}
